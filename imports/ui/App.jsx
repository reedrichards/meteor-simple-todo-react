import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';

import { Tasks } from '../api/tasks.js';

import Task from './Task';
import AccountsUIWrapper from "./AccountsUIWrapper";


const taskMap = task => <Task key={task._id} task={task} />
// App component - represents the whole app
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      hideCompleted: false
    };
  }

  render() {
    return (
      <div className="container">
        <header>
          <h1>Todo List ({this.props.incompleteCount})</h1>

        </header>
        <label className="hide-completed">
          <input
            type="checkbox"
            readOnly
            checked={this.state.hideCompleted}
            onClick={() => this.setState({ hideCompleted: !this.state.hideCompleted })}
          />
            Hide Completed Tasks
          </label>

        <AccountsUIWrapper />
        {this.props.currentUser ?
          <form className="new-task"
            onSubmit={() => {
              Tasks.insert({
                text: this.state.value,
                createdAt: new Date(),
                owner: Meteor.userId(),           // _id of logged in user
                username: Meteor.user().username,  // username of logged in user
              })
            }} >
            <input
              type="text"
              ref="textInput"
              value={this.state.value}
              onChange={(event) => this.setState({ value: event.target.value })}
              placeholder="Type to add new tasks"
            />
          </form>
          : ''}
        <ul>
          {this.state.hideCompleted ?
            this.props.tasks.filter(task => !task.checked).map(taskMap)
            : this.props.tasks.map(taskMap)}
        </ul>
      </div >
    );
  }
}

export default withTracker(() => {
  return {
    tasks: Tasks.find({}, { sort: { createdAt: -1 } }).fetch(),
    incompleteCount: Tasks.find({ checked: { $ne: true } }).count(),
    currentUser: Meteor.user(),

  };
})(App);
